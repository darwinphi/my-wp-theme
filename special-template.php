<?php 

/**
 * Template Name: Special Layout
 */

get_header(); ?>

<?php if ( have_posts() ): ?>
	<?php while ( have_posts() ): the_post(); ?>
		
		<article class="post post__page">
			<h2><?php the_title(); ?></h2>

			<div class="info-box">
				<h4 class="info-box__title">Disclaimer Title</h4>
				<p class="info-box__content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam quas et sapiente alias mollitia.</p>
			</div>

			<p><?php the_content(); ?></p>
		</article>

	<?php endwhile; ?>
<?php else: ?>
	<p>No Content Found</p>
<?php endif; ?>

<?php get_footer(); ?>