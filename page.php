<?php get_header(); ?>

<div class="site-container clearfix">

	<div class="main-column">
		
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ): the_post(); ?>
				
				<?php get_template_part('content', get_post_format()); ?>

			<?php endwhile; ?>
			
		<?php else: ?>

			<p>No Content Found</p>

		<?php endif; ?>

	</div>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>

