<?php get_header(); ?>

<div class="site-container clearfix">
	
	<?php if ( have_posts() ): ?>

		<?php while ( have_posts() ): the_post(); ?>
			
			<h3>Custom HTML</h3>

			<?php the_content(); ?>

			<h3>Custom HTML</h3>

			<div class="home-columns clearfix">

				<div class="one-half">
					<!-- Opinion Posts Loop -->
					<?php 
						// Gets the ID of the category
						$opinionPosts = new WP_Query('cat=10&posts_per_page=2&orderby=title&order=ASC');
					?>

					<?php if ( $opinionPosts->have_posts() ): ?>

						<?php while ( $opinionPosts->have_posts() ): $opinionPosts->the_post(); ?>

							<h2>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h2>
							<?php the_excerpt(); ?>
						<?php endwhile; ?>

					<?php else: ?>

					<?php endif; ?>

					<?php wp_reset_postdata(); ?>

				</div>

				<div class="one-half">
					<!-- Opinion Posts Loop -->
					<?php 
						// Gets the ID of the category
						$opinionPosts = new WP_Query('cat=10&posts_per_page=2&orderby=title&order=ASC');
					?>

					<?php if ( $opinionPosts->have_posts() ): ?>

						<?php while ( $opinionPosts->have_posts() ): $opinionPosts->the_post(); ?>

							<h2>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h2>
							<?php the_excerpt(); ?>
						<?php endwhile; ?>

					<?php else: ?>

					<?php endif; ?>

					<?php wp_reset_postdata(); ?>

				</div>

				
			</div>
			
			

		<?php endwhile; ?>

	<?php else: ?>

		<p>No Content Found</p>

	<?php endif; ?>

</div>

<?php get_footer(); ?>


