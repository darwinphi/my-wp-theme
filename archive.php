<?php get_header(); ?>

<?php if ( have_posts() ): ?>
	
	<!-- Archive Title -->
	<h2>
		<?php 

			if ( is_category() ) {
				single_cat_title();
			} elseif ( is_tag() ) {
				single_tag_title();
			} elseif ( is_author() ) {
				the_post();
				echo "Author Archives: " . get_the_author();
				rewind_posts();
			} elseif ( is_month() ) {
				echo 'Monthly Archives: ' . get_the_date('F Y');
			} elseif ( is_year() ) {
				echo 'Yearly Archives: ' . get_the_date('Y');
			} elseif ( is_date() ) {
				echo 'Daily Archives: ' . get_the_date();
			} else {

			}

		?>
	</h2>

	<?php while ( have_posts() ): the_post(); ?>
		
		<?php get_template_part('content', get_post_format()); ?>

	<?php endwhile; ?>

	<?php echo paginate_links(); ?>

<?php else: ?>

	<p>No Content Found</p>

<?php endif; ?>


<?php get_footer(); ?>

