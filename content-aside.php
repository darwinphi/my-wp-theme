<article class="post post__aside">
	<div class="post-aside__meta">
		<?php the_author(); ?> @ <?php the_time('F j, Y'); ?>
	</div>
	<div class="post-aside__content">
		<?php the_content(); ?>
	</div>
</article>