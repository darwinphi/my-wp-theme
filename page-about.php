<?php get_header(); ?>

<div class="site-container clearfix">

		<?php if ( have_posts() ): ?>

			<?php while ( have_posts() ): the_post(); ?>

				<h1 class="post__title"><?php the_title(); ?></h1>

				<?php the_content(); ?>

			<?php endwhile; ?>

		<?php else: ?>

			<p>No Content Found</p>

		<?php endif; ?>

		<h1>About Us</h1>

		<?php 

			if ( has_post_format( 'about' ) ) {
				echo 'Has about';
			} else {
				echo 'No About';
			}

			$ourCurrentPage = get_query_var( 'paged' );

			$aboutPosts = new WP_Query(array(
				'category_name' 	=> 'about',
				'posts_per_page' 	=> 3,
				'paged'				=> $ourCurrentPage
			));

			if ( $aboutPosts->have_posts() ) :
				while ( $aboutPosts->have_posts() ) :
					$aboutPosts->the_post();
			?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php 
				endwhile;

				previous_posts_link();
				next_posts_link('Next Page', $aboutPosts->max_num_pages);
				echo paginate_links(array(
					'total' => $aboutPosts->max_num_pages
				));

			endif;

		?>

</div>

<?php get_footer(); ?>

