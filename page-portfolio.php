<?php get_header(); ?>

<?php if ( have_posts() ): ?>
	<?php while ( have_posts() ): the_post(); ?>
		
		<article class="post post__page">
		
			<div class="column-container clearfix">

				<div class="column-title">
					<h2><?php the_title(); ?></h2>
				</div>

				<div class="column-content">
					<p><?php the_content(); ?></p>
				</div>

			</div>

		</article>

	<?php endwhile; ?>
<?php else: ?>
	<p>No Content Found</p>
<?php endif; ?>

<?php get_footer(); ?>